let curentSlide = 0;
let distance = 0;
let amount = -1;
let widthSlide = document.querySelector('.slider').offsetWidth;


document.querySelectorAll('.slide').forEach(item => {
    amount++;
});

const next = document.querySelector('.next');
const prev = document.querySelector('.prev');

function controlBtns(){
    if(curentSlide == amount){
        next.classList.add('notActiveBtn');
    }else{
        next.classList.remove('notActiveBtn');
    }
    if(curentSlide == 0){
        prev.classList.add('notActiveBtn');
    }else{
        prev.classList.remove('notActiveBtn');
    }
} 

controlBtns();

next.onclick = () =>{
    curentSlide++;
    document.querySelector('.wrapper').style.transform = `translateX(-${widthSlide * curentSlide}px)`;
    distance = 300*curentSlide;
    controlBtns();
};
prev.onclick = () =>{
    curentSlide--;
    document.querySelector('.wrapper').style.transform = `translateX(-${curentSlide * (widthSlide)}px)`;
    controlBtns();
};